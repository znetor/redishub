#coding: utf-8
from PyQt4 import QtGui
import redis
from PyQt4.QtGui import *
import pickle
from PyQt4.QtCore import *
import sys
import main_ui
import about_ui

class RedisView(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        self.ui=main_ui.Ui_MainWindow()
        self.ui.setupUi(self)
        self.setWindowTitle('RedisView')
        self.ui.connButton.clicked.connect(self.connRedisServer)
        self.ui.filterButton.clicked.connect(self.filterItem)
        self.connect(self.ui.tree,
                SIGNAL("itemSelectionChanged()"),
                self.showItem)

        self.ui.deleteButton.setEnabled(False)
        self.ui.deleteButton.clicked.connect(self.deleteItem)

        self.connect(self.ui.action_4, SIGNAL("triggered()"), self.about)
        try:
            self.conf = pickle.load(open('redisview.dat','r'))
            self.ui.host.setText(self.conf['host'])
            self.ui.port.setText(self.conf['port'])
            self.ui.db.setText(self.conf['db'])
            self.ui.password.setText(self.conf['password'])
        except: pass

    def connRedisServer(self):
        self.conf = {'host':self.ui.host.text(),
                        'port':int(self.ui.port.text()),
                        'db':int(self.ui.db.currentText()),
                        'password':self.ui.password.text()
                        }
        self.ui.db.clear()

        try:
            r = redis.Redis(**self.conf)
            if r:
                self.statusBar().showMessage(u'连接成功')
                self.dbsize = str(r.dbsize())
                self.ui.dbsize.setText(self.dbsize)
        except Exception,e:
            #print e
            self.statusBar().showMessage(u'连接失败, 请检查redis服务器地址和端口号')
            return
        types = set()
        self.ui.tree.clear()
        for i in r.keys():
            item = QTreeWidgetItem(self.ui.tree)
            item.setText(0,r.type(i))
            item.setText(1,i)
            self.ui.tree.addTopLevelItem(item)
            types.add(item.text(0))
        self.r = r
        self.ui.types.clear()
        self.ui.types.addItem('All')
        for t in types:
            self.ui.types.addItem(t)
        self.ui.serverinfo.clear()
        infos =sorted(r.info().iteritems(), key=lambda d:d[0])

        for info in infos:
            item = QTreeWidgetItem(self.ui.serverinfo)
            item.setText(0, info[0])
            item.setText(1, str(info[1]))
            self.ui.serverinfo.addTopLevelItem(item)
            if info[0].startswith('db'):
                if self.ui.db.currentText() != info[0][2:]:
                    self.ui.db.addItem(info[0][2:])
        pickle.dump(self.conf, open('redisview.dat','w'))

    def showItem(self):
        self.ui.deleteButton.setEnabled(True)
        item = self.ui.tree.currentItem()
        data_type = item.text(0)
        data_key = item.text(1)
        if data_type == 'string':
            displayText = self.r.get(data_key)
        elif data_type == 'list':
            displayText = str(self.r.lrange(data_key,0,-1))
        self.ui.itemValue.setText(displayText)

        if self.r.ttl(data_key):
            self.ui.expire.setText(str(self.r.ttl(data_key)))
        else: self.ui.expire.setText(u'永不过期')

    def filterItem(self):
        filter_str = self.ui.filter.text()

        filter_type = self.ui.types.currentText()
        if not filter_str and filter_type == 'All': return
        if not filter_str:
            self.ui.filter.setText('**')
            filter_str = '**'
        keys = self.r.keys(filter_str)
        if not filter_type == 'All':
            keys = [k for k in keys if self.r.type(k) == filter_type]

        self.ui.tree.clear()
        for i in keys:
            item = QTreeWidgetItem(self.ui.tree)
            item.setText(0,self.r.type(i))
            item.setText(1,i)
            self.ui.tree.addTopLevelItem(item)
        self.ui.dbsize.setText(str(len(keys))+'/'+self.dbsize)

    def deleteItem(self):
        curItem = self.ui.tree.currentItem()
        key = curItem.text(1)
        reply = QMessageBox.question(self, u'删除key',
            u"确定删除 "+key+' ?', QMessageBox.Yes , QMessageBox.No)
        if reply == QtGui.QMessageBox.Yes:
            self.r.delete(key)
            self.ui.tree.takeTopLevelItem(self.ui.tree.indexOfTopLevelItem(curItem))

    def about(self):
        self.about = QDialog()
        self.about.ui=about_ui.Ui_Dialog()
        self.about.ui.setupUi(self.about)
        self.about.exec_()

def main():
    app = QtGui.QApplication(sys.argv)
    window = RedisView()
    window.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()